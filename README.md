
![](https://gitlab.com/ddokk/gfx/-/raw/main/g/b1.png)

---

<h1 align="center"><code> Documenting Sources </code></h1>
<h2 align="center"><i> Sources mentioned - https://youtu.be/5ZwY7TjE4o8 </i></h2>

----
1. [Repo Description](#repo-description)
   1. [Video](#video)
2. [Addendum](#addendum)
3. [Resources](#resources)

----

# Repo Description 

Additional documented sources mentioned in the video about the scientific benefits of fasting, in the following video

## Video 

[![](./t/t.png)](https://youtu.be/5ZwY7TjE4o8)

# Addendum 

1. While there are multiple studies on the subject of fasting in general and its numerous metabolic benefits.
2. It is important to note that this is from an Empirical Worldview, in which the Spiritual Component is absent.
3. It is advisable to understand the material and DYOR on how to it related to aspects of Islam
4. Objective of this note is to inform the reader that various scientific proofs have only now come into existence after sufficient advancement in the sciences, which indicates that the traditions of the prophet(PBUH) are far more advanced that our human understanding.

# Resources 


*This will be limited to 5 articles which seem interesting to me, it is upto the reader to investigate this further*

1. [(2023) **HealthLine Website** -  8 Health Benefits of Fasting, Backed by Science](https://www.healthline.com/nutrition/fasting-benefits)
2. [(NoDate) **JohHopkins**  - Intermittent Fasting: What is it, and how does it work?](https://www.hopkinsmedicine.org/health/wellness-and-prevention/intermittent-fasting-what-is-it-and-how-does-it-work)
3. [(2022) **BBC Future** - Intermittent fasting offers the tantalizing promise that changing mealtimes, and not the meals, can be good for you. But what are the dos and don’ts of eating less frequently?](https://www.bbc.com/future/article/20220110-the-benefits-of-intermittent-fasting-the-right-way)
4. [(2021) **Harvard Health Publishing** - Intermittent fasting: The positive news continues](https://www.health.harvard.edu/blog/intermittent-fasting-surprising-update-2018062914156)
5. [(2021) **Nourish by WebMD** - Psychological Benefits of Fasting](https://www.webmd.com/diet/psychological-benefits-of-fasting)